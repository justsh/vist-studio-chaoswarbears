import os
import shutil
import pymel.core as pm
import chaoswarbears
from pipeline.utils import copyutils
from pipeline.maya import checklist
from pipeline.maya.mayautils import mprint


class PublishOutput(object):

    def __init__(self):
        self.console = []
        self.display = []

    def append(self, msg, gui=True):
        self.console.append(msg)
        if gui:
            self.display.append(msg)

    def to_string(self, delimiter='\n'):
        return delimiter.join(self.console)

    def to_display(self, delimiter='\n'):
        return delimiter.join(self.display)

    def clear(self, console=False):
        self.display = []
        if console:
            self.console = []


def bool_str(bool):
    return "Passed" if bool is True else "Failed"


def is_in_feet():
    return (pm.currentUnit(query=True, linear=True) == "ft")


def preflight():

    isEmpty = checklist.is_empty()
    if isEmpty:
        mprint("Aborting Checklist. File is empty")
        return False

    isSaved = checklist.is_saved()
    if not isSaved:
        mprint("Aborting Checklist. File has never been saved")
        return False

    isWorkingTree = chaoswarbears.project.validate()
    isGrouped = checklist.is_grouped()
    usingProperShaders = checklist.shader_check()
    usingProperNames = checklist.default_names_check()
    usingFeet = is_in_feet()
    hasHistory = checklist.has_history(remove=False)

    pm.select(clear=True)
    output = PublishOutput()

    output.append("\n"*3, gui=False)
    output.append("-"*80, gui=False)

    output.append("## Required Checklist", gui=False)
    output.append("{0:50}: {1} ".format("Is the current file in the \"working/\" tree?",
                                        bool_str(isWorkingTree)))
    output.append("{0:50}: {1} ".format("Are the current file units set to \"feet\"?",
                                        bool_str(usingFeet)))
    output.append("\n")
    output.append("{0:50}: {1} ".format("Is the top hierarchy grouped?",
                                        bool_str(isGrouped)))
    output.append("{0:50}: {1} ".format("Are objects using non-default names?",
                                        bool_str(usingProperNames)))
    output.append("{0:50}: {1} ".format("Are objects using proper shaders?",
                                        bool_str(usingProperShaders)))

    if not usingProperShaders:
        output.append("-- Objects should not use lambert1")

    output.append("\n")
    summary = output.to_display()
    output.clear()

    output.append("!! Warnings", gui=False)
    output.append("{0:50}: {1} ".format("History Deleted",
                                        bool_str(not hasHistory)))

    output.append("\n")
    warnings = output.to_display()
    output.clear()

    ready_for_publish = (isWorkingTree and
                         isGrouped and
                         usingProperNames and
                         usingProperShaders)

    output.append("** Result", gui=False)
    if ready_for_publish:
        output.append("Passed checklist", gui=False)
    else:
        output.append("Failed checklist", gui=False)

    output.append("-"*80, gui=False)
    output.append("\n", gui=False)

    output_terminal = output.to_string()
    print(output_terminal)

    gui_cb = pm.Callback(gui, summary, warnings, ready_for_publish)

    if ready_for_publish:
        result = pm.layoutDialog(title="Publish Checklist | Passed", ui=gui_cb)
    else:
        result = pm.layoutDialog(title="Publish Checklist | Failed", ui=gui_cb)

    if result == "Publish":
        publish_result = publish()
        if publish_result:
            w_path, r_path = publish_result

            asset_dir = os.path.dirname(w_path)
            asset_name = os.path.basename(asset_dir)
            asset_type_dir = os.path.dirname(asset_dir)
            asset_type = os.path.basename(asset_type_dir)
            mprint("Published working {0} {1} to release {1}".format(
                asset_name, asset_type))
    else:
        mprint("See script editor for Publish Checklist Results")


def publish():
    # Copy working file to release
    r_filepath = w_filepath = pm.sceneName()

    replaces = (("working", "release"),
               ("assets", "published_assets"),
               ("w-", "r-"))

    for r in replaces:
        r_filepath = r_filepath.replace(r[0], r[1])

    r_dirpath = os.path.dirname(r_filepath)
    copyutils.make_sure_path_exists(r_dirpath)

    try:
        shutil.copy(w_filepath, r_filepath)
        return (w_filepath, r_filepath)
    except Exception as e:
        print e

    return False


def gui(summary_txt, warning_txt, result):

    import getpass

    dismiss = lambda *args: pm.layoutDialog(dismiss="Dismiss")
    publish = lambda *args: pm.layoutDialog(dismiss="Publish")
    usercheck = lambda: getpass.getuser() == "justin" or getpass.getuser() == "Matt"

    form = pm.setParent(query=True)
    pm.formLayout(form, edit=True, width=480)

    col = pm.columnLayout(adjustableColumn=True,
                          columnAttach=('both', 10),
                          rowSpacing=10)

    pm.text(label="Checklist Summary", font="boldLabelFont", align="left")
    pm.separator(style="none", height=8)

    pm.text(label="Required", align="left")
    pm.separator()

    pm.scrollLayout(horizontalScrollBarThickness=0,
                    verticalScrollBarThickness=16,
                    childResizable=True,
                    height=160)
    pm.text(label=summary_txt, align="left", font="smallFixedWidthFont", wordWrap=True)
    pm.setParent(upLevel=True)

    pm.separator(style="none", height=8)
    pm.text(label="Warnings", align="left")
    pm.separator()

    pm.scrollLayout(horizontalScrollBarThickness=0,
                    verticalScrollBarThickness=16,
                    childResizable=True,
                    height=80)

    pm.text(label=warning_txt, align="left", font="smallFixedWidthFont", wordWrap=True)
    pm.setParent(upLevel=True)
    pm.separator(style="none", height=8)

    pm.rowLayout(numberOfColumns=2,
                 adjustableColumn=1,
                 columnAttach=[(1, 'left', 5),
                               (2, 'both', 5)])

    pm.text(label="Final Result:", align="right", font="boldLabelFont")
    result_txt = "Passed" if result else "Failed"
    pm.text(label=result_txt, align="right")

    pm.setParent(upLevel=True)
    pm.separator()

    pm.rowLayout(numberOfColumns=2,
                 adjustableColumn=2,
                 columnAttach=[(1, 'left', 5),
                               (2, 'both', 5)])

    pm.button("Dismiss", command=dismiss, width=120)
    pm.button("Publish", command=publish, width=100, enable=(result or usercheck()))
    pm.setParent(upLevel=True)

    pm.formLayout(form,
                  edit=True,
                  attachForm=[(col, 'top', 15),
                              (col, 'left', 5),
                              (col, 'right', 5),
                              (col, 'bottom', 15)],
                  )
