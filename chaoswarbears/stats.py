import json
from multiprocessing import cpu_count
from getpass import getuser
from socket import gethostname

from ..utils import statutils


def get_stats():
    stats = {}

    m = statutils.MemoryCheck()

    stats["username"] = getuser()
    stats["computer"] = gethostname()
    stats["cpucores"] = cpu_count()
    stats["swap"] = m.memstats["TotalPageFile"]
    stats["ram"] = m.memstats["TotalPhys"]

    print(stats)
