import os
from ..chaoswarbears import project
from pipeline.maya import rigutils
from pipeline.utils import copyutils


session_last_saved_weights = None


def save():
    global session_last_saved_weights

    asset = "stag"
    dagAsset = "*:stagBody"

    weights_dir = os.path.join(project.path,
                               "working",
                               "assets",
                               "weights",
                               asset)
    copyutils.make_sure_path_exists(weights_dir)

    files = [f for f in os.listdir(weights_dir)]
    file_count = len(files)
    file_count_str = ""

    file_count_str = "_{0:03}".format(file_count + 1)

    filename = ''.join([asset, "_weights", file_count_str, ".json"])
    filepath = os.path.join(weights_dir, filename)

    weighted_vertices = rigutils.get_weights(dagAsset)
    rigutils.save_weights(weighted_vertices, filepath)

    session_last_saved_weights = weighted_vertices

    return weighted_vertices


def gui():
    pass
