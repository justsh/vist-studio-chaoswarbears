import pymel.core as pm


def load_render_settings():

    # drg.listAttr() will tell you everything you need
    drg = pm.PyNode("defaultRenderGlobals")

    # Output format 'custom format'
    drg.imageFormat.set(51)

    # Output format OpenEXR
    drg.imfkey.set("exr")
    drg.imfPluginKey.set("exr")

    drg.extensionPadding.set(4)

    # True: put an underscore before the frame padding
    # False: put a period before the frame padding
    drg.putFrameBeforeExt.set(True)

    # Possible values are 0, 1, and 2
    # This setting is used in combination with putFrameBeforeExt
    drg.periodInExt.set(2)
    drg.imageFilePrefix("<Scene>/<Camera>/<Scene>_<RenderLayer>")

    try:
        mrg = pm.PyNode("mentalrayGlobals")
    except pm.MayaNodeError:
        if not pm.renderer('mentalRay', exists=True):
            pm.loadPlugin("Mayatomr", quiet=True)

        drg.currentRenderer.set("mentalRay")
        mrg = pm.PyNode("mentalrayGlobals")

    # OpenEXR Zip Compression
    mrg.imageCompression.set(4)

    dfb = pm.PyNode("miDefaultFramebuffer")
    # RGBA (Short) 4x16
    dfb.datatype.set(3)

    ast = pm.playbackOptions(q=True, ast=True)
    aet = pm.playbackOptions(q=True, aet=True)

    drg.startFrame.set(ast)
    drg.endFrame.set(aet)
    drg.byFrame.set(1)

    # Allows a custom buffer name
    drg.multiCamNamingMode.set(True)
    # Sets the custom buffer name
    drg.bufferName.set("<RenderPassType>:<RenderPass>")


def render_layers():
    from pipeline.maya import renderutils as ru
    from pipeline.maya.renderutils import RL

    # Pass Name: pass id name
    passes_dict = {
        "diffuseMaterialColor": "DIFRAW",
        "mv2dToxik": "MV2E",
        "specNoShadow": "SPECNS"
    }

    render_layers = {
        "passes": RL("passes", None, passes_dict, True, None),
        "ldepth": RL("ldepth", "linearDepth", None, False, None)
    }

    sel = []
    sel = pm.ls(sl=True)

    ru.create_render_passes(passes_dict)
    ru.create_render_layers(render_layers, sel)
    ru.associate_passes(render_layers)
