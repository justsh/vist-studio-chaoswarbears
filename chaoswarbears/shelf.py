
import pymel.core as pm
import os
from pipeline.maya import guitools
from pipeline.maya.mayautils import mprint


normalize_join = lambda *args: os.path.normpath(os.path.join(*args))


def shelf_contents():
    blank_counter = guitools.SpacerUI()

    maya_appdir = pm.internalVar(userAppDir=True)
    icondir = normalize_join(maya_appdir, "scripts", "icons")

    if not os.path.exists(icondir):
        icondir = ""

    print_tpl = "import pymel.core as pm; pm.displayInfo(\"{msg}\")"
    deferred_tpl = ("import pymel; pymel.mayautils.executeDeferred(\"{0}\")")

    # commands to execute
    # NOTE: There is no reload for the following commands.
    # For now, only the menu should reload
    setCamAttrs_CMD = ("from chaoswarbears import cameras; "
                       "cameras.set_camera_clips()")

    pubChecklist_CMD = ("from chaoswarbears import publish; "
                        "publish.preflight()")

    saveBackupVersion_CMD = ("from pipeline.maya.saveutils import SaveCommands; "
                             "SaveCommands.promptSaveBackupVersion()")

    validateWorkspace_CMD = ("import chaoswarbears; "
                             "chaoswarbears.project.validate()")

    installShelf_CMD = deferred_tpl.format("from chaoswarbears import shelf; "
                                           "reload(shelf)")

    # lets create a list of dictionaries for the buttons and walk through that,
    # this is all the buttons you want to build, just add more as needed.
    # the top most item is your left most button
    shelf_buttons = [
        {
            "label": "chaosWarTeddy",
            "image": normalize_join(icondir, "teddy_bear.png"),
            "command": print_tpl.format(msg="WARBEAR!"),
        },
        {
            "label": blank_counter.spacer(),
            # spacer does nothing but fill in spaces
            "annotation": "",
        },
        {
            "label": "setCameraAttrs",
            #"image": "menuIconView.png",
            "image": "defaultTwoStackedLayout.png",
            "command": setCamAttrs_CMD,
            "annotation": "Adjust near clipping planes "
                          "of animated cameras",
            "imageOverlayLabel": "cams",
        },
        {
            "label": "pubChecklist",
            #"image": "renderDiagnostics.png",
            "image": "defaultTwoStackedLayout.png",
            "command": pubChecklist_CMD,
            "annotation": "Run Publish Checklist on current file",
            "imageOverlayLabel": "pub",
        },
        {
            "label": blank_counter.spacer(),
            "annotation": "",
        },
        {
            "label": "saveBackupVersion",
            "image": "defaultTwoStackedLayout.png",
            "command": saveBackupVersion_CMD,
            "annotation": "Save Backup Version",
            "imageOverlayLabel": "save",
        },
        {
            "label": blank_counter.spacer(),
            "annotation": "",
        },
        {
            "label": "validateWorkspace",
            "image": "defaultTwoStackedLayout.png",
            "command": validateWorkspace_CMD,
            "annotation": "Validate the Current Project (workspace)"
                          "against expected workspaces",
            "imageOverlayLabel": "proj",
        },
        {
            "label": blank_counter.spacer(),
            "annotation": "",
        },
        {
            "label": "installChaosWarShelf",
            "image": "reload.png",
            "command": installShelf_CMD,
            "annotation": "Rebuild the ChaosWarShelf",
        },
    ]

    return shelf_buttons


shelf_buttons = shelf_contents()
shelf = guitools.MayaShelf("ChaosWarShelf")
shelf.move(1)
shelf.buildUI(shelf_buttons)
mprint("chaoswarbears shelf built at {0}".format(shelf.name))
