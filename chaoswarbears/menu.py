import os
from pipeline.maya import guitools
from pipeline.maya.mayautils import mprint


menu_name = "ChaosWarMenu"
menu = guitools.MayaMenu(menu_name, menu_name)
menu.buildUI(os.path.join(os.path.dirname(__file__), menu_name))
mprint("chaoswarbears menu built at {0}".format(menu.name))


"""
OLD

menu_tree = OrderedDict([
    ("label", "ChaosWarMenu"),
    ("common_items", [
        {
            "label": "Save Backup Version",
            "command": saveBackupVersion_cmd,
            "annotation": "Save an incremented backup of the current file",
        },
        {
            "label": "Save As Copy",
            "command": saveAsCopy_cmd,
            "annotation": "Save a copy, but keep editing the current file",
        },
        {
            "divider": True,
        },
        {
            "label": "Publish",
            "command": pubChecklist_cmd,
            "annotation": "Run Publish Checklist on current file",
        },
        {
            "divider": True,
        },
    ]),
    ("rutil_submenu", OrderedDict([
        ("label", "Render Utilities"),
        ("rutil_items", [
            {
                "label": "Set Camera Clips",
                "command": setCameraClips_cmd,
                "annotation": "Adjust near clipping planes "
                              "of animated cameras",
            },
            {
                "label": "Visibility Override",
                "command": visibilityOverride_cmd,
                "annotation": "Override the visibility of "
                              "selection/filter for the current "
                              "render layer",
            },
            {
                "label": "Create Passes",
                "command": createPasses_cmd,
                "annotation": "Create common render layers and "
                              "optionally associate passes",
            },
            {
                "label": "Set Render Settings",
                "command": setRenderSettings_cmd,
                "annotation": "Adjust render settings for "
                              "OpenEXR workflow",
            },
            {
                "label": "Fix Render View",
                "command": "source \"{0}\";".format("fixRenderView"),
                "annotation": "Fixes RenderView not found error",
                "sourceType": "mel",
            },
        ]),
    ])),
    ("debug_items", [
        {
            "divider": True,
        },
        {
            "label": "Rebuild This Menu",
            "command": installCWBMenu_cmd,
            "annotation": "Rebuild the ChaosWarMenu",
        },
        {
            "label": "Rebuild ChaosWarShelf",
            "command": installChaosWarShelf_cmd,
            "annotation": "Rebuild the ChaosWarShelf",
        },
        {
            "label": "Update Scripts",
            "command": updateScripts_cmd,
            "annotation": "Update Scripts from the Repository"
        }
    ]),
])
"""
