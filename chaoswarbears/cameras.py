import pymel.core as pm
import pymel.core.nodetypes as nt


def set_camera_clips(distance=0.01):

    cameras = pm.ls(type=nt.Camera)
    cameras[0].name()
    for cam in cameras:
        if "cam_" in cam.name():
            cam.setNearClipPlane(distance)
