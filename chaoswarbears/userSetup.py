import sys

# Prevent caching to avoid Dropbox conflicts when Windows or OSX
# try to rewrite each other's cache files
# sys.dont_write_bytecode = True


import pymel.core as pm
from pymel import mayautils as _mayautils


def try_defer(func, *args):
    def wrapper(*args):
        try:
            func()
        except ImportError as e:
            print("Chaoswarbears: " + str(e))
    return wrapper


@try_defer
def load_save_reminder():
    deferred_str = ("from pipeline.common import save_reminder;")
    _mayautils.executeDeferred(deferred_str)


@try_defer
def commandPort_SublimeText():
    """ Open command port 7002 for SublimeText """
    from pipeline.maya import mayautils

    mayautils.port(port="7002", sourceType="python")


@try_defer
def load_SaveCommands():
    """ Add Save Backup Increment and Save A Copy to the File Menu """

    deferred_str = ("from pipeline.maya import saveutils; "
                    "saveutils.SaveCommands().buildUI()")
    _mayautils.executeDeferred(deferred_str)


@try_defer
def load_ChaosWarBearShelf():
    """ Rebuild the ChaosWarBear Shelf """
    deferred_str = ("from chaoswarbears import shelf")
    _mayautils.executeDeferred(deferred_str)


@try_defer
def load_ChaosWarBearMenu():
    """ Build the ChaosWarBear Menu """
    deferred_str = ("from chaoswarbears import menu")
    _mayautils.executeDeferred(deferred_str)


load_save_reminder()
load_SaveCommands()
load_ChaosWarBearShelf()
load_ChaosWarBearMenu()
