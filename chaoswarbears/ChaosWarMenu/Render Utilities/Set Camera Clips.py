import pymel.core as pm
import pymel.core.nodetypes as nt

cameras = pm.ls(exactType=nt.Camera)
for cam in cameras:
    if not cam.isOrtho() and cam.renderable.get():
        cam.setNearClipPlane(0.01)
