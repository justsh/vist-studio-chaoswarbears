import os
import pymel.core as pm
from pipeline.maya import cluster_gen


normalized_join = lambda *args: os.path.normpath(os.path.join(*args))

transforms_filepath = normalized_join("D:\\", "Dropbox", "ChaosWarBears",
                                      "_chaoswarbears", "maya", "transforms.txt")

if not os.path.exists(transforms_filepath):
    raise IOError("Transformation file does not exist")


def export():

    pm.select([])

    selector = "tree_*:*"
    sel = pm.ls(selector, transforms=True, long=True)
    sel = sorted(set(pm.listRelatives(sel, parent=True)))

    cluster_gen.export_transforms(sel,
                                  transforms_filepath)


def populate():

    pm.select([])

    refdict = {}
    refdict["namespace"] = "tree"
    refdict["namespace_tpl"] = '_'.join([refdict["namespace"], "{0:04d}"])
    refdict["group_as"] = "reftree_GRP"

    cluster_gen.populate_transforms(refdict,
                                    transforms_filepath)
