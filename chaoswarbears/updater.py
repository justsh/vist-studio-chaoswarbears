import os
import dropfinder
from pipeline.utils import copyutils
from pipeline.maya.mayautils import mprint


normalize_join = lambda *args: os.path.normpath(os.path.join(*args))

SOURCES = [
    os.path.join("D:\\",
                 "local",
                 "vist-studio-chaoswarbears",
                 "chaoswarbears"),
    os.path.join("D:\\",
                 "local",
                 "vist-studio-chaoswarbears",
                 "externals",
                 "dropfinder"),
    os.path.join("D:\\",
                 "local",
                 "vist-studio-chaoswarbears",
                 "externals",
                 "pipeline-tools",
                 "pipeline")
]

# Replace this join with the Workspace Utilities
drpb_cwb = normalize_join(dropfinder.locate(),
                          "ChaosWarBears",
                          "_chaoswarbears",
                          "maya",
                          "scripts",
                          "vist-studio-chaoswarbears")

#DESTS = [drpb_cwb] * len(SOURCES)
DESTS = [normalize_join(drpb_cwb, "chaoswarbears"),
         normalize_join(drpb_cwb, "dropfinder"),
         normalize_join(drpb_cwb, "pipeline")]


def blacklist_function(filepath):
    return (not filepath.endswith(".pyc")
            and not filepath.endswith(".md")
            and not "sublime-" in filepath
            and not ".git" in filepath)


# TODO: updater should return the number of files updated for convenience
def update_scripts(clean=False):

    oldlogger = copyutils.log
    copyutils.log = mprint

    source_dirs = SOURCES
    destination_dirs = DESTS
    blacklist = blacklist_function

    if clean:
        for dstdir in destination_dirs:
            copyutils.recursive_delete(dstdir)

    results = []

    for srcdir, dstdir in zip(source_dirs, destination_dirs):
            r = copyutils.cptree(srcdir, dstdir,
                                 copyutils.smart_copy,
                                 filter_by=blacklist)
            results.append((r, srcdir, dstdir))

    copyutils.log = oldlogger

    for count, srcdir, destdir in results:
        if count:
            mprint("Updated", count, "files from", srcdir, "to", dstdir)
        else:
            mprint("No update required")
