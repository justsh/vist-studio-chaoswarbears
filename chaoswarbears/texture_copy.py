import os
from pipeline.utils import copyutils

normalized_join = lambda *args: os.path.normpath(os.path.join(*args))

PATH_BASE = os.path.join("D:", "Dropbox", "ChaosWarBears", "_chaoswarbears")

SRC_DIR_ROOT = normalized_join(PATH_BASE, "working", "assets", "model")
DEST_DIR_ROOT = normalized_join(PATH_BASE, "textures")


def move_to_mirrored_tree(src, dest, child_dir=None):

    src_dirs = [p for p in os.listdir(src)
                if os.path.isdir(normalized_join(src, p))]

    warnings = []

    for p in src_dirs:
        #print(p, os.path.basename(p))
        src_dir = normalized_join(src, p, "textures")
        dest_dir = normalized_join(dest, p)

        if not os.path.exists(src_dir):
            warnings.append(
                "Warning: source path \"{0}\" does not exist".format(
                src_dir))
            continue

        if child_dir:
            dest_dir = normalized_join(dest_dir, child_dir)

        """
        print("moving dirtree from root \"{0}\" to \"{1}\"".format(
            src_dir, dest_dir))
        """

        copyutils.cptree(src_dir, dest_dir, move=True)

    print("\n\n{0} warnings encountered".format(len(warnings)))
    for warning in warnings:
        print(warning)


def main():
    move_to_mirrored_tree(SRC_DIR_ROOT, DEST_DIR_ROOT)


if __name__ == "__main__":
    main()
