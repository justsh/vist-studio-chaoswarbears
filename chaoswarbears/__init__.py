# Define Project Settings
import os
import dropfinder
import pymel.core as pm

from pipeline.maya.project import MayaProject
from pipeline.maya import mayatomr as mr


project = MayaProject("ChaosWarBears",
                      os.path.join(dropfinder.locate(),
                                   "ChaosWarBears",
                                   "_chaoswarbears"),
                      onValidate=("import chaoswarbears; "
                                  "chaoswarbears.project.validate()"))


project.add_callback(
    "MentalRayTextureSettings",
    pm.Callback(mr.mi_set_texture_opt_settings,
                cache=True,
                scope=mr.mi_CACHE_ALL_TEXTURES,
                mode=mr.mi_USE_CUSTOM_CACHE,
                cdir=os.path.join(pm.Workspace().getPath(),
                                  "maya",
                                  "sourceimages",
                                  "cache")
                ),
    run_once=True
)
