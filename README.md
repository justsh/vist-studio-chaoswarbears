# Chaoswarbears VIST Project
Team: C
Team Name: ChaosWarBears
Aliases: Chaoswarbears, _chaoswarbears, Chaos Warbears, War Bears
Battle Cry: WARBEAR!

The name ChaosWarBear is a mashup of Geoff's love for War Bears and Justin's arbitrary selection of the word "chaos" from a pool of words that started with the letter C (for 'team c')

Team ChaosWarBears is a part of the first year of the Vertical Studio at Texas A&M University Department of Visualization. The goal of the project is to allow members from sophomore, junior, and senior studios to collaborate on group projects, simultaneously giving seniors the opportunity to lead disciplines as technical and artistic directors


## Fall 2012
### Leads (405):
Chelsey Gobeli
Wes Cardwell
Schaefer Mitchell
Justin Sheehy

### Juniors (305):
Geoff "G-off" Dunn
Jeff Nichols
Ashley Lane
Kathy Pangtay

### Sophomores (205):
Dani Crowley
Emma Mercado
Austin Ratliff
Jessie Ayers
Daniel House
Tanner Hladek
Courtney Champion

### Honorary Warbears
Channing Williford
Kay-Leigh Farley


## Spring 2013
### Leads (406):
Justin Sheehy
Matthew Hurley
Collin Seiffert

### Juniors (305):
David Torres
Bailee Stooksberry
Jamie Lynn Smith

### Sophomores (206):
Dani Crowley
Emma Mercado
Austin Ratliff
Christian Diaz
Josh Lemon